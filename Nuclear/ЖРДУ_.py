from latexlate.Template import Template
import sys, io,os
import sympy
#--------------------------------------------------------------------------------------------




from sympy.printing.latex import print_latex




mo = 160
mg = 40
Kmj = 4
Kmv = 0.6
ro = 1590
rg = 845
RT = 49e4
ht=0.4
hn=0.6
k=1.2

mtj = mo + mo/Kmj
mtv = mg + mg*Kmv
dpgg=2.94e6
dpk = 1.4716
pvh=0.49e6

Qs = mo/ro+mg/rg
pi_t = sympy.symbols('\\pi_t')
Ntj = print_latex(mtj*ht*RT*k/(k-1)*(1-pi_t**((1-k)/k)))
























#--------------------------------------------------------------------------------------------

with io.open(sys.argv[1], 'r', encoding="utf-8") as file:
    text = ''.join(file.readlines())
t = Template(text, globals())


out = t.render()

with io.open(sys.argv[2], 'w', encoding="utf-8") as file:
    file.write(out)
