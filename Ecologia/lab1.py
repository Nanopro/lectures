from latexlate.Template import Template
import sys, io,os
import sympy
#--------------------------------------------------------------------------------------------


t=10
mt=1

x_co=0.117
x_no2=0.32e-5
x_no=0.007



m=mt*t

m_co = x_co*m
m_no2 = x_no2*m
m_no = x_no*m


S1=100
h1=3

V1 = S1* h1 
 
c_co1 = m_co / V1 *1e6
c_no21 = m_no2 / V1 *1e6
c_no1 = m_no / V1 *1e6


S2=300
h2=5

V2 = S2 * h2 

c_co2 = m_co / V2 *1e6
c_no22 = m_no2 / V2 *1e6
c_no2 = m_no / V2 *1e6



p_co = 20
p_no2 = 2
p_no = 5

V3= max([m_co * 1e6 / p_co, m_no * 1e6 / p_no,m_no2 * 1e6 / p_no2,])











#--------------------------------------------------------------------------------------------

with io.open(sys.argv[1], 'r', encoding="utf-8") as file:
    text = ''.join(file.readlines())
t = Template(text, globals())


out = t.render()

with io.open(sys.argv[2], 'w', encoding="utf-8") as file:
    file.write(out)
