from latexlate.Template import Template
import sys, io,os
import re

import math as m
import numpy as np
from scipy import optimize, interpolate

import matplotlib.pyplot as plt
T_a=275
T_b=285
p_rab=3e6
dp_vh=0.9e6
m_a=2.5
tau=300
dp_dr_a=0.075e6
dp_dr_b=0.07e6
p_nch_b=28e6
l_a=5
l_b=11
ksi_a=4
ksi_b=5
W_a=7
rho_a=811

d_shtr_a=((4*m_a)/(np.pi*rho_a*W_a))**0.5
d_a=24e-3
p_pr=1.5*p_rab
sigma_t=300e6
delta_tr_shtr_A=(p_pr*d_a)/(2*sigma_t)
delta_tr_A=0.0005
W_a_fix=(4*m_a)/(np.pi*rho_a*d_a**2)

nu_a=7.5e-4

Re_a=rho_a*W_a*d_a/nu_a

lambda_a=1/((1.81*np.log10(Re_a)-1.64)**2)


dp_m_nad=rho_a*W_a**2/2 *(lambda_a*l_a/d_a+ksi_a)

dp_m_nad_fix=dp_m_nad*1.3
p_pod_A=p_rab+dp_vh+dp_m_nad_fix+rho_a*W_a**2/2+dp_dr_a

Vk_rab=m_a*tau/rho_a

Vzapr_doz_a=1.35*Vk_rab

V_BA=1.2*Vzapr_doz_a

Q_a=m_a/rho_a

W_b=12

d_shtr_b=((4*Q_a)/(np.pi*W_b))**0.5
d_b=20e-3
p_pr_b=1.5*p_nch_b
delta_tr_shtr_B=(p_pr_b*d_b)/(2*sigma_t)
delta_tr_B=0.0015

W_b_fix=(4*Q_a)/(np.pi*d_b**2)

rho_b=p_pod_A/(8314/4*T_b)
nu_b=18.64e-6

Re_b=rho_b*W_b*d_b/nu_b

lambda_b=1/((1.81*np.log10(Re_b)-1.64)**2)

dp_m_nad_b=rho_b*W_b**2/2 *(lambda_b*l_b/d_b+ksi_b)

p_vyh_red=p_pod_A+1.3*dp_m_nad_b+rho_b*W_b**2/2+dp_dr_b

p_vh_red=1.5*p_vyh_red

p_b=p_vh_red

c_1=0.71
c_2=0.87

V_b=(p_pod_A*V_BA*c_1/c_2)/(c_1*p_nch_b-(p_b+0.5*p_vyh_red))


m_b=(p_nch_b*V_b)/(8314/4*T_b)

d_ba=(12*V_BA/17/np.pi)**(1/3)

R_ba=d_ba/2

k=2

delta_bak_a=p_vyh_red*k*R_ba/sigma_t

m_bb=Q_a*rho_b
d_bb=(12*V_b/17/np.pi)**(1/3)

R_bb=d_bb/2

delta_bak_b=p_nch_b*k*R_bb/sigma_t

F_b=np.pi*d_b**2/4

malph=Q_a/(F_b*(2*dp_dr_b/rho_b)**0.5)

sqrtm=0.32
mm=sqrtm**2

d_0=sqrtm*d_b


eps = 1 - (0.41 + 0.35 * mm**2)*dp_dr_b/(1.666*p_vyh_red)

D1 = 10**3*malph*eps*F_b*(dp_dr_b*p_pod_A/(8314/4*T_b))**(0.5)/(2**(0.5)*dp_dr_b)*0.5

D2 = 10**5*malph*eps*F_b*(dp_dr_b*p_pod_A/(8314/4*T_b))**(0.5)/(2**(0.5)*p_pod_A)*0.4

D3 = 10*malph*eps*F_b*(dp_dr_b*p_pod_A/(8314/4*T_b))**(0.5)/(2**(0.5)*T_b)*1

D = (D1**2+D2**2+D3**2)**(0.5)

delta = D/m_bb



with io.open(sys.argv[1], 'r', encoding="utf-8") as file:
    text = ''.join(file.readlines())
t = Template(text, globals())
out = t.render()

with io.open(sys.argv[2], 'w', encoding="utf-8") as file:
    file.write(out)