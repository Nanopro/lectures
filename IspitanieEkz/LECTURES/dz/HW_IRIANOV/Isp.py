from latexlate.Template import Template
import sys, io,os
import numpy as np
import math
from sympy.solvers import solve as slv
from sympy import Symbol as smb
import matplotlib.pyplot as plt
import pylab
from scipy import optimize
import scipy.interpolate


################# Исходные данные ####################

Ta = 278
Tb = 280
p_rab = 4*10**6
dp_vh = 0.5*10**6
ma = 0.7
t = 360
dp_dra = 0.08*10**6
dp_drb = 0.07*10**6
p_nachb = 30*10**6
La = 8
Lb = 9
Xsi_a = 8
Xsi_b = 8 


Sigma_t = 529*10**6 ### Зависит от компонента А
Nu_a = 6.45*10**(-7)
rho_a = 786

Mu_b = 19/10**6

################## Расчёт расходной магистрали А ########################


### 1. Геометрические размеры ###

Wa1 = 5

da1 = ((4*ma)/(np.pi*rho_a*Wa1))**(0.5)

da = round(da1, 3)

Wa = 4*ma/(np.pi*rho_a*da**2)

delta1 = (1.5*p_rab*da)/(2*Sigma_t)

delta = 1*10**(-3)

### 2. Гидравлические потери ###

Re_a = Wa*da/Nu_a

Ld_a = 1/(1.81*math.log10(Re_a)-1.64)**2

dp_a1 = rho_a*Wa**2/2 * (Ld_a*(La/da)+Xsi_a)

dp_a = 1.3*dp_a1

### 3. Определение давления подачи на наддув бака А ###

pa_pod = p_rab + dp_vh + dp_a + dp_dra + rho_a*Wa**2/2

### 4. Определение заправочной дозы компонента А ###

Va_zapr = 1.4*(ma*t/rho_a)

### 5. Определение объёма расходной ёмкости ###

Va_bak = 1.2*Va_zapr



################## Расчёт расходной магистрали Б ########################

### 1. Геометрические размеры ###

Wb1 = 10

db1 = ((4*ma)/(np.pi*rho_a*Wb1))**(0.5)

db = round(db1, 3)

Wb = 4*ma/(np.pi*rho_a*db**2)

### 2. Гидравлические потери ###

rho_b = pa_pod/(8314/28*Tb)

Re_b = rho_b*Wb*db/Mu_b

Ld_b = 1/(1.81*math.log10(Re_b)-1.64)**2

dp_b1 = rho_b*Wb**2/2 * (Ld_b*(Lb/db)+Xsi_b)

dp_b = 1.3*dp_b1

### 3. Давление на выходе из резервуара ###

p_vihred = pa_pod + dp_b + dp_drb + rho_b*Wb**2/2

### 4. Давление на входе в редуктор ###

p_vhred = 1.5*p_vihred

### 5. Давление в баллоне ###

p_ball = p_vhred

### 6. Объём баллона ###

C1 = 0.82
C2 = 0.9

V_ball =  (p_rab * Va_bak * (C1/C2))/(C1*p_nachb - 10**5)

### 7. Запас сжатого газа ###

Mb = (p_nachb * V_ball)/(8314/28*Tb)


################## Расчёт дроссельного расходомера ########################

### 1. Диаметр ###

eps = 1

mb = Mb/t

malf = 4*mb/(eps*np.pi*da**2*(2*rho_b*dp_drb)**(0.5))

alf = 0.64

fi = 0.5

Re_gr50 = 7*10**4

Re_gr = Re_gr50*db/0.05

m = malf/alf

F0 = m*np.pi*db**2/4

d0 = (4*F0/np.pi)**(0.5)

eps = 1 - (0.41 + 0.35 * m**2)*dp_drb/(1.4*p_vihred)

### 2. Погрешности ###

F1 = np.pi*da**2/4

D1 = 10**3*malf*eps*F1*(dp_drb*pa_pod/(8314/28*Tb))**(0.5)/(2**(0.5)*dp_drb)*0.5

D2 = 10**5*malf*eps*F1*(dp_drb*pa_pod/(8314/28*Tb))**(0.5)/(2**(0.5)*pa_pod)*0.4

D3 = 10*malf*eps*F1*(dp_drb*pa_pod/(8314/28*Tb))**(0.5)/(2**(0.5)*Tb)*1

D = (D1**2+D2**2+D3**2)**(0.5)

delta = D/mb
 


 


with io.open(sys.argv[1], 'r', encoding="utf-8") as file:
    text = ''.join(file.readlines())
t = Template(text, globals())
out = t.render()

with io.open(sys.argv[2], 'w', encoding="utf-8") as file:
    file.write(out)